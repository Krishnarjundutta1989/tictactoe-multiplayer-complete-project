//
//  ViewController.m
//  TestTicTacToe
//
//  Created by Anil Kumar on 9/22/15.
//  Copyright (c) 2015 Anil Kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    
    NSMutableArray *array1stUserSelectedButtons;
    NSMutableArray *array2ndUserSelectedButtons;
    NSMutableArray *arrayAllSelectedButtons;
    
    NSMutableArray  *btnArray;
    UIImageView * defaultImageView;
    UIAlertView *alert;
    
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    {
        
        btnArray = [NSMutableArray new];
        
        array1stUserSelectedButtons = [NSMutableArray new];
        array2ndUserSelectedButtons = [NSMutableArray new];
        arrayAllSelectedButtons = [NSMutableArray new];
        
        
        UIImageView *imgTicTac = [UIImageView new];
        imgTicTac.frame = CGRectMake(20, 150, 350, 320);
        imgTicTac.image = [UIImage imageNamed:@"puzzle_game_best_tic_tac_toe_for_iphone_ipod_touch_and_ipad_1.png"];
        [self.view addSubview:imgTicTac];
        
        int xPos = 70;
        int yPos = 180;
        
        for (int i=1; i<=9; i++) {
            
            UIView *viewTicTac = [UIView new];
            viewTicTac.frame = CGRectMake(xPos, yPos, 67, 80);
            viewTicTac.backgroundColor = [UIColor redColor];
            
            UIButton *btnTicTac = [UIButton new];
            btnTicTac.frame = CGRectMake(0, 0, 67, 80);
            btnTicTac.backgroundColor = [UIColor whiteColor];
            btnTicTac.tag = i;
            [btnTicTac addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            xPos = xPos + 83;
            
            if (i%3 == 0) {
                
                xPos = 70;
                yPos = yPos + 95;
            }
            [self.view addSubview:viewTicTac];
            [viewTicTac addSubview:btnTicTac];
            [btnArray addObject:btnTicTac];
        }
    }
}

- (void)btnClicked:(UIButton *)sender {
    [arrayAllSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
    
    
    if ([arrayAllSelectedButtons count] == 1)  {
        [array1stUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    }
    
    else if ([arrayAllSelectedButtons count] == 2){
        [array2ndUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
        
    }
    else if ([arrayAllSelectedButtons count] == 3){
        [array1stUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    }
    else if ([arrayAllSelectedButtons count] == 4){
        [array2ndUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
    }
    else if ([arrayAllSelectedButtons count] == 5){
        [array1stUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        
        
        if (([array1stUserSelectedButtons containsObject:@"1"] && [array1stUserSelectedButtons containsObject:@"2"] && [array1stUserSelectedButtons containsObject:@"3"]) || ([array1stUserSelectedButtons containsObject:@"4"] && [array1stUserSelectedButtons containsObject:@"5"] && [array1stUserSelectedButtons containsObject:@"6"])||([array1stUserSelectedButtons containsObject:@"7"] && [array1stUserSelectedButtons containsObject:@"8"] && [array1stUserSelectedButtons containsObject:@"9"])||([array1stUserSelectedButtons containsObject:@"1"] && [array1stUserSelectedButtons containsObject:@"4"] && [array1stUserSelectedButtons containsObject:@"7"])||([array1stUserSelectedButtons containsObject:@"2"] && [array1stUserSelectedButtons containsObject:@"5"] && [array1stUserSelectedButtons containsObject:@"8"]) || ([array1stUserSelectedButtons containsObject:@"3"] && [array1stUserSelectedButtons containsObject:@"6"] && [array1stUserSelectedButtons containsObject:@"9"])||([array1stUserSelectedButtons containsObject:@"1"] && [array1stUserSelectedButtons containsObject:@"5"] && [array1stUserSelectedButtons containsObject:@"9"])||([array1stUserSelectedButtons containsObject:@"3"] && [array1stUserSelectedButtons containsObject:@"5"] && [array1stUserSelectedButtons containsObject:@"7"]))
        {
                alert = [[UIAlertView alloc] initWithTitle:@"Congratulation" message:@"1st Player Won" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
        }
        
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    }
    else if ([arrayAllSelectedButtons count] == 6){
        [array2ndUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        
        if (([array2ndUserSelectedButtons containsObject:@"1"] && [array2ndUserSelectedButtons containsObject:@"2"] && [array2ndUserSelectedButtons containsObject:@"3"]) || ([array2ndUserSelectedButtons containsObject:@"4"] && [array2ndUserSelectedButtons containsObject:@"5"] && [array2ndUserSelectedButtons containsObject:@"6"])||([array2ndUserSelectedButtons containsObject:@"7"] && [array2ndUserSelectedButtons containsObject:@"8"] && [array2ndUserSelectedButtons containsObject:@"9"])||([array2ndUserSelectedButtons containsObject:@"1"] && [array2ndUserSelectedButtons containsObject:@"4"] && [array2ndUserSelectedButtons containsObject:@"7"])||([array2ndUserSelectedButtons containsObject:@"2"] && [array2ndUserSelectedButtons containsObject:@"5"] && [array2ndUserSelectedButtons containsObject:@"8"]) || ([array2ndUserSelectedButtons containsObject:@"3"] && [array2ndUserSelectedButtons containsObject:@"6"] && [array2ndUserSelectedButtons containsObject:@"9"])||([array2ndUserSelectedButtons containsObject:@"1"] && [array2ndUserSelectedButtons containsObject:@"5"] && [array2ndUserSelectedButtons containsObject:@"9"])||([array2ndUserSelectedButtons containsObject:@"3"] && [array2ndUserSelectedButtons containsObject:@"5"] && [array2ndUserSelectedButtons containsObject:@"7"]))

        {
            alert = [[UIAlertView alloc] initWithTitle:@"Congratulation" message:@"2nd Player Won" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
    }
    else if ([arrayAllSelectedButtons count] == 7){
        [array1stUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        
        if (([array1stUserSelectedButtons containsObject:@"1"] && [array1stUserSelectedButtons containsObject:@"2"] && [array1stUserSelectedButtons containsObject:@"3"]) || ([array1stUserSelectedButtons containsObject:@"4"] && [array1stUserSelectedButtons containsObject:@"5"] && [array1stUserSelectedButtons containsObject:@"6"])||([array1stUserSelectedButtons containsObject:@"7"] && [array1stUserSelectedButtons containsObject:@"8"] && [array1stUserSelectedButtons containsObject:@"9"])||([array1stUserSelectedButtons containsObject:@"1"] && [array1stUserSelectedButtons containsObject:@"4"] && [array1stUserSelectedButtons containsObject:@"7"])||([array1stUserSelectedButtons containsObject:@"2"] && [array1stUserSelectedButtons containsObject:@"5"] && [array1stUserSelectedButtons containsObject:@"8"]) || ([array1stUserSelectedButtons containsObject:@"3"] && [array1stUserSelectedButtons containsObject:@"6"] && [array1stUserSelectedButtons containsObject:@"9"])||([array1stUserSelectedButtons containsObject:@"1"] && [array1stUserSelectedButtons containsObject:@"5"] && [array1stUserSelectedButtons containsObject:@"9"])||([array1stUserSelectedButtons containsObject:@"3"] && [array1stUserSelectedButtons containsObject:@"5"] && [array1stUserSelectedButtons containsObject:@"7"]))
            
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Congratulation" message:@"1st Player Won" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

        }
        
        
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    }
    else if ([arrayAllSelectedButtons count] == 8){
        [array2ndUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        
        if (([array2ndUserSelectedButtons containsObject:@"1"] && [array2ndUserSelectedButtons containsObject:@"2"] && [array2ndUserSelectedButtons containsObject:@"3"]) || ([array2ndUserSelectedButtons containsObject:@"4"] && [array2ndUserSelectedButtons containsObject:@"5"] && [array2ndUserSelectedButtons containsObject:@"6"])||([array2ndUserSelectedButtons containsObject:@"7"] && [array2ndUserSelectedButtons containsObject:@"8"] && [array2ndUserSelectedButtons containsObject:@"9"])||([array2ndUserSelectedButtons containsObject:@"1"] && [array2ndUserSelectedButtons containsObject:@"4"] && [array2ndUserSelectedButtons containsObject:@"7"])||([array2ndUserSelectedButtons containsObject:@"2"] && [array2ndUserSelectedButtons containsObject:@"5"] && [array2ndUserSelectedButtons containsObject:@"8"]) || ([array2ndUserSelectedButtons containsObject:@"3"] && [array2ndUserSelectedButtons containsObject:@"6"] && [array2ndUserSelectedButtons containsObject:@"9"])||([array2ndUserSelectedButtons containsObject:@"1"] && [array2ndUserSelectedButtons containsObject:@"5"] && [array2ndUserSelectedButtons containsObject:@"9"])||([array2ndUserSelectedButtons containsObject:@"3"] && [array2ndUserSelectedButtons containsObject:@"5"] && [array2ndUserSelectedButtons containsObject:@"7"]))
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Congratulation" message:@"2nd Player Won" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
    }
    
    else if ([arrayAllSelectedButtons count] == 9){
        [array1stUserSelectedButtons addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
        UIButton *btn = (UIButton *) sender;
        [btn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end